package com.ace.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ace.recyclerview.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val listColonist = arrayListOf(
            MyColonist(1, R.mipmap.person1,"Ace", 20, "Researcher"),
            MyColonist(1, R.mipmap.person2,"Ilham", 20, "Farmer"),
            MyColonist(1, R.mipmap.person3,"Uus", 18, "Doctor"),
        )

        val adapter = ColonistAdapter(listColonist)

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)

        recyclerView.layoutManager = layoutManager

        recyclerView.adapter = adapter
    }
}