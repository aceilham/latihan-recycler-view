package com.ace.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView



class ColonistAdapter(private val listColonist: ArrayList<MyColonist>)
    : RecyclerView.Adapter<ColonistAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val avatar = itemView.findViewById<ImageView>(R.id.vAvatar)
        val name = itemView.findViewById<TextView>(R.id.tvName)
        val age = itemView.findViewById<TextView>(R.id.tvAge)
        val profession = itemView.findViewById<TextView>(R.id.tvProfession)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.my_colony_profiles, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = listColonist[position]
        holder.avatar.setImageResource(data.photo)
        holder.name.text = "${data.name}"
        holder.age.text = "${data.age} years old"
        holder.profession.text = "${data.profession}"
    }

    override fun getItemCount(): Int {
        return listColonist.size
    }
}
