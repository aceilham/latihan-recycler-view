package com.ace.recyclerview

data class MyColonist(
    val id:Int,
    val photo: Int,
    val name:String,
    val age:Int,
    val profession:String)